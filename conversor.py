# pesos = input("¿Cuántos pesos colombianos tienes? ")
# pesos = float(pesos)
# valor_dolar = 3.875
# dolares = pesos / valor_dolar 
# dolares = round(dolares, 2)
# dolares = str(dolares)
# print("Tienes $" + dolares + " dólares")

def conversor(tipo_peso, valor_dolar):
    pesos = float(input("¿Cuántos pesos " + tipo_peso + " tienes? "))
    valor = valor_dolar
    dolares = round(pesos / valor, 2)
    print("Tienes $" + str(dolares) + " dólares")


menu = """
BIENVENIDO AL CONVERSOR DE MODEDAS

1 - Pesos Colombianos 
2 - Pesos Argentinos
3 - Pesos Mexicanos 

Elige una opción: """

opcion = int(input(menu)) 

if opcion == 1: 
    conversor("Colombianos", 3875)
    # pesos = float(input("¿Cuántos pesos tienes? "))
    # valor_dolar = 3875
    # dolares = round(pesos /valor_dolar, 2)
    # print("Tienes $" + str(dolares)+ " dólares")
elif opcion == 2:
    conversor("Argentinos", 65)
    # pesos = float(input("¿Cuántos pesos tienes? "))
    # valor_dolar = 65
    # dolares = round(pesos /valor_dolar, 2)
    # print("Tienes $" + str(dolares)+ " dólares")
elif opcion == 3:
    conversor("Mexicanos", 24)
    # pesos = float(input("¿Cuántos pesos tienes? "))
    # valor_dolar = 24
    # dolares = round(pesos /valor_dolar, 2)
    # print("Tienes $" + str(dolares)+ " dólares")
else:
    print("Ingresa una opción correcta")


