#Escribe un programa que pregunte al usuario por el número de horas trabajadas y el coste por hora. 
# Después debe mostrar por pantalla la paga que le corresponde.

def calcular(horas, costo):
    pago = horas * costo
    print("Su pago es de " + str(pago))

horas = int(input("Ingresa tus horas de trabajo: "))
costo = int(input("Cuánto cobras por hora? "))

calcular(horas,costo)