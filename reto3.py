#Preguntar al usuario su nombre e imprimir su respuesta en letras mayúsculas y decirle cuántas letra tiene su nombre

def nombres(nombre: str) -> int: 
    return len(nombre.replace(" ",""))

nombre: str = input("¿Cuál es tu nombre? ")
letras = nombres(nombre)
print(nombre.upper() + " Tiene " + str(letras) + " letras")