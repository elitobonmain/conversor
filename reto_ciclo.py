# Escribir un programa que pida al usuario una palabra y la muestre por pantalla 10 veces.

def run():
    palabra = input("Escribe una palabra")
    LIMITE = 10
    contador = 0
    while contador < LIMITE:
        print(palabra)
        contador = contador +1

if __name__ == "__main__":
    run()